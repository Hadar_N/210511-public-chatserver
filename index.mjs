import express from 'express';
import http from 'http';
import { Server } from "socket.io";

const app = express();
const server = http.createServer(app);

const io = new Server(server);

app.get('/', (req, res) => {
  //res.sendFile(process.cwd() + '/index.html');
  res.status(200).send("server started");
});

io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('disconnect', () => {
        console.log('user disconnected');
      });

      socket.on('chat message', (msg) => {
        io.emit('chat message', msg);
      });
  });


server.listen(3030, () => {
  console.log('listening on localhost:3030');
});